from dataclasses import dataclass, field, replace
from typing import Dict

from wow_decision_nodes.player import Player


@dataclass
class GameState:
    player: Player
    number_of_enemies: int
    number_of_allies: int
    cooldowns: Dict[str, float] = field(default_factory=dict)

    def update_cooldowns(self, current_time):
        return {k: v for k, v in self.cooldowns.items() if v >= current_time}

    def legal_spells(self):
        return [k for k in self.player.spells if k not in self.cooldowns]

    def copy(self):
        return replace(self, cooldowns=dict(**self.cooldowns))

@dataclass
class DiscPriestGameState(GameState):
    mana: int = 50000
