from typing import Optional, Dict

from mcts.decision_node import DecisionNode
from wow_decision_nodes.value import WowValue
from wow_decision_nodes.wow_state import WowState


class WowDecisionNode(DecisionNode):

    def __init__(self, action: str, state: WowState, parent: Optional["WowDecisionNode"] = None):
        self.action = action
        self.state: WowState = state
        self.parent = parent
        self.children: Dict[str, "WowDecisionNode"] = {}
        self.number_of_visits = 0
        self.value = WowValue()

    def expand(self):
        action = self.untried_actions.pop()
        next_state = self.state.action(action)
        child_node = WowDecisionNode(action, next_state, parent=self)
        self.children[action] = child_node
        return child_node
