import random
from random import choice
from typing import List, Optional, Dict

import numpy as np

from mcts.base_state import BaseState


class DecisionNode:
    _untried_actions: List[str]

    def __init__(self, action: str, state: BaseState, parent: Optional["DecisionNode"] = None):
        self.action = action
        self.state: BaseState = state
        self.parent = parent
        self.children: Dict[str, "Node"] = {}
        self.number_of_visits = 0
        self.value = 0

    def __repr__(self):
        return f"<Node {self.action} {self.avg_value} over {self.number_of_visits} visits>"

    @property
    def untried_actions(self):
        if not hasattr(self, "_untried_actions"):
            self._untried_actions = self.state.get_legal_actions()
        return self._untried_actions

    @property
    def is_fully_expanded(self):
        return len(self.untried_actions) == 0

    def expand(self):
        action = self.untried_actions.pop()
        next_state = self.state.copy()
        next_state.action(action)
        child_node = DecisionNode(action, next_state, parent=self)
        self.children[action] = child_node
        return child_node

    def rollout(self):
        current_rollout_state = self.state
        while not current_rollout_state.is_game_over():
            possible_moves = current_rollout_state.get_legal_actions()
            action = choice(possible_moves)
            current_rollout_state = current_rollout_state.action(action)
        return current_rollout_state.value

    def is_terminal_node(self):
        return self.state.is_game_over()

    def backpropagate(self, result):
        self.number_of_visits += 1
        self.value += result
        if self.parent:
            self.parent.backpropagate(result)

    @property
    def avg_value(self):
        if not self.number_of_visits:
            return 0
        return self.value / self.number_of_visits

    def count_factor(self, parent_visits):
        return np.sqrt(2 * np.log(parent_visits) / self.number_of_visits)

    def best_action(self, c_param=0.0):
        if self.number_of_visits < len(self.children) * 5:
            return random.choice(list(self.children.keys()))
        choices_weights = {
            action: c.avg_value + c_param * c.count_factor(self.number_of_visits)
            for action, c in self.children.items() if c.number_of_visits
        }
        return max(choices_weights, key=choices_weights.get)

    def best_child(self, c_param=1):
        return self.children[self.best_action(c_param)]

    def predicted_best(self):
        if self.is_terminal_node():
            return [], self.state.value
        if not self.children:
            return ["..."], self.avg_value
        action = self.best_action()
        actions, value = self.children[action].predicted_best()
        return [action] + actions, value

    def simulate(self, number_of_iterations):
        for i in range(number_of_iterations):
            v = self._tree_policy()
            reward = v.rollout()
            v.backpropagate(reward)

    def _tree_policy(self):
        current_node = self
        while not current_node.is_terminal_node():

            if not current_node.is_fully_expanded:
                return current_node.expand()
            else:
                current_node = current_node.best_child()
        return current_node
