from dataclasses import dataclass


@dataclass
class FightConfig:
    length_of_fight: int
    number_of_enemies: int
    number_of_party: int
