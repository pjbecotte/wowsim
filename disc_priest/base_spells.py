from functools import partial
from math import floor
from typing import TYPE_CHECKING

from main import SpellResult


if TYPE_CHECKING:
    from disc_priest import DiscPriest
    from main import FriendlyTarget, EnemyTarget


class DiscPriestSpell:
    mana_cost_percent: float
    cast_time: float = 1.5
    cooldown: float = 0
    # a spell like mindblast that does damage and prevents damage will have both of these set positive
    healing_spell_power_modifier: float = 0
    damage_spell_power_modifier: float = 0
    # a spell that hits 5 players should have number and aoe_max ADD UP TO 5
    number_hits: int = 1
    aoe_max_hits: int = 0
    trigger_atonement: bool = False
    applies_atonement: int = 0
    shadow: bool = False

    def __get__(self, player: "DiscPriest", owner):
        return partial(self.cast, player)

    def cast(self, player, target):
        healing = self.get_healing(player, target=target)
        damage = self.get_damage(player, target=target)
        if self.trigger_atonement:
            healing = floor(
                healing
                + (damage * 0.5 * player.mastery_modifier * player.current_atonement)
            )
        cast_time = self.cast_time / player.haste_modifier
        mana_cost = floor(self.mana_cost_percent * player.base_mana)
        return SpellResult(
            self.__class__.__name__,
            healing=healing,
            damage=damage,
            cast_time=cast_time,
            mana_cost=mana_cost,
        )

    @staticmethod
    def healing_modifier(player: "DiscPriest"):
        return (
            player.stats.intellect
            * player.versatility_modifier
            * player.crit_modifier
            * player.mastery_modifier
            * player.twist_of_fate_modifier
            * player.shadow_covenant_modifier
        )

    @staticmethod
    def damage_modifier(player: "DiscPriest"):
        return (
            player.stats.intellect
            * player.versatility_modifier
            * player.twist_of_fate_modifier
            * player.shadow_covenant_modifier
            * player.schism_modifier
            * player.sins_of_the_many_modifier
        )

    def get_healing(self, player: "DiscPriest", **kwargs) -> int:
        return floor(
            self.healing_modifier(player)
            * self.healing_spell_power_modifier
            * (min(self.aoe_max_hits, player.fight.aoe_targets) + self.number_hits)
        )

    def get_damage(self, player: "DiscPriest", **kwargs) -> int:
        return floor(
            self.damage_modifier(player)
            * self.damage_spell_power_modifier
            * (min(self.aoe_max_hits, player.fight.aoe_targets) + self.number_hits)
        )
