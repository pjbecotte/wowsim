from dataclasses import dataclass

from simulation.actor import Actor


@dataclass
class FriendlyTarget(Actor):
    pass
