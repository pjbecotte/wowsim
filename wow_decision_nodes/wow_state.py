from typing import Dict, List, Optional

from mcts.base_state import BaseState
from mcts.config import config
from wow_decision_nodes.action import Action
from wow_decision_nodes.aura import Aura
from wow_decision_nodes.value import WowValue


class WowState(BaseState):
    def __init__(self, gamestate, elapsed: float = 0.0, value: WowValue = None, auras: Optional[Dict[str, List[Aura]]] = None):
        super().__init__(elapsed=elapsed, value=value or WowValue(healing=0, damage=0))
        self.auras: Dict[str, List[Aura]] = auras or {}
        self.gamestate = gamestate

    def copy(self):
        auras = {k: v.copy() for k, v in self.auras.items()}
        gamestate = self.gamestate.copy()
        return WowState(gamestate, self.elapsed, self.value.copy(), auras)

    def action(self, action: str):
        chosen_action = self.get_action(action)
        aura_value = self.resolve_auras(chosen_action.cast_time)
        cast_value = chosen_action.execute(self.auras)
        if chosen_action.cooldown:
            self.gamestate.cooldowns[action] = self.elapsed + chosen_action.cooldown
        self.elapsed += chosen_action.gcd
        self.value += aura_value + cast_value


    def get_legal_actions(self):
        self.gamestate.update_cooldowns(self.elapsed)
        return self.gamestate.legal_spells()

    def is_game_over(self):
        return self.elapsed >= config.max_time

    def resolve_auras(self, cast_time):
        value = WowValue()
        for aura_name, aura_instances in self.auras.items():
            for aura_instance in aura_instances:
                value += aura_instance.resolve(self.elapsed + cast_time)
        for aura_name in self.auras:
            self.auras[aura_name] = [a for a in self.auras[aura_name] if a.expires_at <= self.elapsed]
        self.auras = {k: v for k, v in self.auras.items() if v}
        return  value

    def get_action(self, action) -> Action:
        return self.gamestate.player.spells[action](self.gamestate)

