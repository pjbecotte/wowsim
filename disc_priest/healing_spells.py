from math import floor
from typing import TYPE_CHECKING

from wow_decision_nodes.action import DiscPriestSpell

if TYPE_CHECKING:
    from disc_priest import DiscPriest


class DefensivePenance(DiscPriestSpell):
    mana_cost_percent = 0.016
    healing_spell_power_modifier = 1.25
    contrition_modifier = 0.13
    cooldown = 9

    def get_healing(self, player: "DiscPriest", **kwargs) -> int:
        number_hits = 4 if player.talents.castigation else 3
        mod = self.healing_modifier(player)
        heal = floor(mod * self.healing_spell_power_modifier * number_hits)
        if player.talents.contrition:
            heal += floor(mod * self.contrition_modifier * player.current_atonement)
        return heal


class Shadowmend(DiscPriestSpell):
    mana_cost_percent = 0.035
    healing_spell_power_modifier = 3.2
    applies_atonement = 15


class PWShield(DiscPriestSpell):
    mana_cost_percent = 0.031
    healing_spell_power_modifier = 1.65
    applies_atonement = 15


class PWRadiance(DiscPriestSpell):
    mana_cost_percent = 0.065
    healing_spell_power_modifier = 1.05
    aoe_max_hits = 4
    applies_atonement = 9
    cooldown = 20.0
