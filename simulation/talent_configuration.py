from dataclasses import dataclass


@dataclass
class DiscTalents:
    castigation: bool = False
    twist_of_fate: bool = False
    schism: bool = False
    shield_discipline: bool = False
    mindbender: bool = False
    solace: bool = False
    sins_of_the_many: bool = False
    contrition: bool = False
    shadow_covenant: bool = False
    purge_the_wicked: bool = False
    divine_star: bool = False
    halo: bool = False
    lenience: bool = False
    spirit_shell: bool = False
    evangelism: bool = False
