from functools import partial


class Ability:
    def __get__(self, player, owner):
        return partial(self.cast, player)

    def __set_name__(self, owner, name):
        owner.abilities[name] = self

    def cast(self, player):
        pass
