from math import floor

from wow_decision_nodes.action import DiscPriestSpell


class Smite(DiscPriestSpell):
    mana_cost_percent = 0.004
    damage_spell_power_modifier = 0.5
    trigger_atonement = True


class MindBlast(DiscPriestSpell):
    mana_cost_percent = 0.025
    damage_spell_power_modifier = 0.9792
    healing_spell_power_modifier = 3
    trigger_atonement = True
    shadow = True


class Penance(DiscPriestSpell):
    mana_cost_percent = 0.016
    damage_spell_power_modifier = 0.4
    cooldown = 9
    trigger_atonement = True

    def get_damage(self, player, **kwargs) -> int:
        number_hits = 4 if player.talents.castigation else 3
        return floor(
            self.damage_modifier(player)
            * self.damage_spell_power_modifier
            * number_hits
        )


class SWPain(DiscPriestSpell):
    mana_cost_percent = 0.003
    # have to handle dots eventually
    damage_spell_power_modifier = 0.1292 + 0.57528
    trigger_atonement = True
    shadow = True


class SWDeath(DiscPriestSpell):
    mana_cost_percent = 0.005

    def get_damage(self, player, **kwargs) -> int:
        return floor(
            self.damage_modifier(player)
            * (0.85 if kwargs.get("target", 100) >= 20 else 1.5)
        )


class Mindgames(DiscPriestSpell):
    mana_cost_percent = -0.02
    damage_spell_power_modifier = 3
    # mindgames aura prevents damage AND heals, double it
    healing_spell_power_modifier = 4.5 * 2
    trigger_atonement = True
    cooldown = 45
