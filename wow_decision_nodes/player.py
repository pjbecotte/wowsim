from dataclasses import dataclass
from typing import ClassVar

from disc_priest.healing_spells import Shadowmend, PWShield, PWRadiance


@dataclass
class Stats:
    intellect: int = 1470
    critical_strike: int = 0
    mastery: int = 0
    haste: int = 0
    versatility: int = 0
    leech: int = 0


@dataclass
class Player:
    """Dunno values to use for any other class, so just copied the priest
    ones for now
    """
    stats: Stats
    base_mana: ClassVar = 50000
    cost_haste: ClassVar = 33
    cost_crit: ClassVar = 35
    cost_mastery: ClassVar = 25.93
    cost_vers: ClassVar = 40
    base_mastery: ClassVar = 1.108
    base_crit: ClassVar = 1.05
    spells: ClassVar = {}

    @property
    def haste_modifier(self):
        return 1 + ((self.stats.haste / self.cost_haste) / 100)

    @property
    def mastery_modifier(self):
        return self.base_mastery + ((self.stats.mastery / self.cost_mastery) / 100)

    @property
    def crit_modifier(self):
        return self.base_crit + ((self.stats.critical_strike / self.cost_crit) / 100)

    @property
    def versatility_modifier(self):
        return 1 + ((self.stats.versatility / self.cost_vers) / 100)

    def get_talents(self, talent_list):
        raise NotImplementedError()



@dataclass
class DiscTalents:
    castigation: bool = False
    twist_of_fate: bool = False
    schism: bool = False
    shield_discipline: bool = False
    mindbender: bool = False
    solace: bool = False
    sins_of_the_many: bool = False
    contrition: bool = False
    shadow_covenant: bool = False
    purge_the_wicked: bool = False
    divine_star: bool = False
    halo: bool = False
    lenience: bool = False
    spirit_shell: bool = False
    evangelism: bool = False

    @classmethod
    def from_list(cls, talent_list):
        return cls(**{talent: True for talent in talent_list})


@dataclass
class DiscPriest(Player):
    base_mana: ClassVar = 50000
    cost_haste: ClassVar = 33
    cost_crit: ClassVar = 35
    cost_mastery: ClassVar = 25.93
    cost_vers: ClassVar = 40
    base_mastery: ClassVar = 1.108
    base_crit: ClassVar = 1.05

    talents: DiscTalents
    spells: ClassVar = {
        "shadowmend": Shadowmend,
        "pw_shield": PWShield,
        "radiance_first": PWRadiance,
        "radiance_second": PWRadiance,
    }

    def get_talents(self, talent_list):
        return DiscTalents(**{talent: True for talent in talent_list})
