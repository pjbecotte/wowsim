from collections import defaultdict

from simulation.actor import Actor


class EnemyTarget(Actor):
    def __init__(self):
        self.debuffs = defaultdict(int)

    def add_damage_event(self, event_time, damage):
        self.dmg_events.add((event_time, damage))
