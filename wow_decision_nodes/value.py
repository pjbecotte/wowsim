class WowValue:
    def __init__(self, damage=0, healing=0):
        self.healing = healing
        self.damage = damage

    def copy(self):
        return WowValue(self.damage, self.healing)

    def __repr__(self):
        return f"<Healing: {self.healing} Damage: {self.damage}"

    def __gt__(self, other):
        return self.healing > other.healing

    def __eq__(self, other):
        return self.healing == other.healing

    def __add__(self, other):
        return WowValue(healing=self.healing + other.healing, damage=self.damage + other.damage)

    def __truediv__(self, other):
        return self.healing / other
