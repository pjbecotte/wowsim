from mcts.config import config


class BaseState:
    def __init__(self, elapsed=0, value=0):
        self.value = value
        self.elapsed = elapsed

    def action(self, action: str):
        payoff = {
            "first": 5,
            "second": 10,
            "third": 15,
        }
        return BaseState(self.elapsed + 1, self.value + payoff[action])

    def get_legal_actions(self):
        actions = ["first", "second", "third"]
        return actions

    def is_game_over(self):
        return self.elapsed >= config.max_time
