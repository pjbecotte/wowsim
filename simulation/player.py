from dataclasses import dataclass

from simulation.actor import Actor


@dataclass
class Player(Actor):
    mana: int

    def possible_actions(self):
        pass
