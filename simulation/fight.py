from dataclasses import dataclass
from random import choice
from typing import List, Iterable

from simulation.actor import Actor
from simulation.enemy_target import EnemyTarget
from simulation.fight_configuration import FightConfig
from simulation.friendly_target import FriendlyTarget
from simulation.player import Player


@dataclass
class Action:
    ability: str
    targets: List[Actor]
    cast_time:


class Fight:
    def __init__(self, player: Player, fight_config: FightConfig):
        self.elapsed_time = 0
        self.fight_config = fight_config
        self.player = player
        self.friendly_targets = [FriendlyTarget() for _ in range(fight_config.number_of_party)]
        self.enemy_targets = [EnemyTarget() for _ in range(fight_config.number_of_enemies)]

    @property
    def all_targets(self) -> Iterable[Actor]:
        for tgt in self.friendly_targets:
            yield tgt
        for tgt in self.enemy_targets:
            yield tgt

    @property
    def finished(self):
        return self.elapsed_time >= self.fight_config.length_of_fight

    def choose_action(self):
        return choice(self.player.possible_actions(self))

    def handle_cast(self, length: float, ability: str, targets: List[Actor]):
        self.elapsed_time += length
        events = self.resolve_auras()
        events.extend(getattr(self.player, ability)(targets))

    def resolve_auras(self):
        events = []
        for actor in self.all_targets:
            events.extend(actor.resolve_auras(self.elapsed_time))
        return events
