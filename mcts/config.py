from dataclasses import dataclass


@dataclass
class Config:
    max_time = 10.0


config = Config()
