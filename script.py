# from disc_priest import DiscPriest
#
# player = DiscPriest("data.yaml")
# casts = []
# smite = player.smite(player.fight.enemy_targets[0])
# def_pen = player.def_penance(player.fight.enemy_targets[0])
# off_pen = player.penance(player.fight.enemy_targets[0])
# bm = player.mind_blast(player.fight.enemy_targets[0])
# mg = player.mindgames(player.fight.enemy_targets[0])
# swp = player.sw_pain(player.fight.enemy_targets[0])
#
# print(smite)
# print(def_pen)
# print(50 * smite.healing + 6 * off_pen.healing + 3 * mg.healing + 4 * bm.healing)
# print(50 * smite.healing + 6 * def_pen.healing + 3 * mg.healing + 4 * bm.healing)
#
# cast_options = [
#     "smite",
#     "penance",
#     "mind_blast",
#     "sw_pain",
#     "sw_death",
#     "mindgames",
# ]
from mcts.base_state import BaseState
from mcts.decision_node import DecisionNode
from wow_decision_nodes.gamestate import DiscPriestGameState
from wow_decision_nodes.player import DiscPriest, DiscTalents, Stats
from wow_decision_nodes.wow_decision_node import WowDecisionNode
from wow_decision_nodes.wow_state import WowState


def main():
    talents = DiscTalents.from_list(["castigation", "mindbender", "sins_of_the_many", "lenience"])
    player = DiscPriest(stats=Stats(), talents=talents)

    gamestate = DiscPriestGameState(player=player, number_of_enemies=1, number_of_allies=5)
    state = WowState(gamestate)

    root = WowDecisionNode("root", state)
    root.simulate(1000)
    print(root.predicted_best())


if __name__ == "__main__":
    main()
