from math import floor
from typing import TYPE_CHECKING

from wow_decision_nodes.value import WowValue


if TYPE_CHECKING:
    from wow_decision_nodes.gamestate import GameState, DiscPriestGameState


class Action:
    base_cast_time: float
    gamestate: "GameState"
    mana_cost_percent: float
    base_cast_time: float = 1.5
    cooldown: float = 0.0
    gcd: float = 0.0

    def __init__(self, gamestate: "GameState"):
        self.gamestate = gamestate

    def execute(self, auras):
        return WowValue()

    @property
    def cast_time(self):
        return self.base_cast_time / self.player.haste_modifier

    @property
    def mana_cost(self):
        return floor(self.mana_cost_percent * self.player.base_mana)

    @property
    def player(self):
        return self.gamestate.player


class DiscPriestSpell(Action):
    gamestate: "DiscPriestGameState"
    # a spell like mindblast that does damage and prevents damage will have both of these set positive
    healing_spell_power_modifier: float = 0
    damage_spell_power_modifier: float = 0
    # a spell that hits 5 players should have number and aoe_max ADD UP TO 5
    number_hits: int = 1
    aoe_max_hits: int = 0
    trigger_atonement: bool = False
    applies_atonement: int = 0
    shadow: bool = False

    def __init__(self, gamestate: "DiscPriestGameState"):
        super().__init__(gamestate)



    def execute(self, auras):
        healing = self.get_healing(auras)
        damage = self.get_damage(auras)
        if self.trigger_atonement:
            healing = floor(
                healing
                + (damage * 0.5 * self.player.mastery_modifier * len(auras.get("atonement", [])))
            )
        return WowValue(damage=damage, healing=healing)

    def healing_modifier(self, auras):
        player = self.gamestate.player
        mod = player.stats.intellect * player.versatility_modifier * player.crit_modifier
        if auras.get("twist_of_fate"):
            mod = mod * 1.2
        if auras.get("shadow_covenant_modifier"):
            mod = mod * 1.25
        return mod

    def damage_modifier(self, auras):
        player = self.gamestate.player
        mod = player.stats.intellect * player.versatility_modifier * player.crit_modifier
        if auras.get("twist_of_fate"):
            mod = mod * 1.2
        if auras.get("schism"):
            mod = mod * 1.25
        if auras.get("shadow_covenant"):
            mod = mod * 1.25
        if player.talents.sins_of_the_many:
            atonements = len(auras.get("atonement", []))
            mod = mod * {
                0: 1.12,
                1: 1.12,
                2: 1.1,
                3: 1.08,
                4: 1.07,
                5: 1.06,
                6: 1.06,
                7: 1.05,
                8: 1.04,
                9: 1.04,
            }.get(atonements, 1.03)
        return mod

    def get_healing(self, auras) -> int:
        return floor(
            self.healing_modifier(auras)
            * self.healing_spell_power_modifier
            * (min(self.aoe_max_hits, self.gamestate.number_of_allies) + self.number_hits)
        )

    def get_damage(self, auras) -> int:
        return floor(
            self.damage_modifier(auras)
            * self.damage_spell_power_modifier
            * (min(self.aoe_max_hits, (self.gamestate.number_of_enemies - 1)) + self.number_hits)
        )
