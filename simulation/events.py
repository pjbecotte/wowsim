from dataclasses import dataclass


@dataclass
class Event:
    name: str
    timestamp: float


@dataclass
class Damage(Event):
    damage_amount: int


@dataclass
class Healing(Event):
    healing_amount: int
