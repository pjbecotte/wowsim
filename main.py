from dataclasses import dataclass
from pathlib import Path
import yaml


@dataclass
class SpellResult:
    spell_name: str
    damage: int = 0
    healing: int = 0
    mana_cost: int = 0
    cast_time: float = 1.5


@dataclass
class Stats:
    intellect: int = 1470
    critical_strike: int = 0
    mastery: int = 0
    haste: int = 0
    versatility: int = 0
    leech: int = 0


class Player:
    """Dunno values to use for any other class, so just copied the priest
    ones for now
    """

    base_mana = 50000
    cost_haste = 33
    cost_crit = 35
    cost_mastery = 25.93
    cost_vers = 40
    base_mastery = 1.108
    base_crit = 1.05

    def __init__(self, config_path):
        self.elapsed_time = 0
        data = yaml.safe_load(Path(config_path).read_text())
        self.stats = Stats(**data["character"]["stats"])
        self.talents = self.get_talents(data["character"]["talents"])
        self.fight = Fight(**data["fight"])

    @property
    def haste_modifier(self):
        return 1 + ((self.stats.haste / self.cost_haste) / 100)

    @property
    def mastery_modifier(self):
        return self.base_mastery + ((self.stats.mastery / self.cost_mastery) / 100)

    @property
    def crit_modifier(self):
        return self.base_crit + ((self.stats.critical_strike / self.cost_crit) / 100)

    @property
    def versatility_modifier(self):
        return 1 + ((self.stats.versatility / self.cost_vers) / 100)

    def get_talents(self, talent_list):
        raise NotImplementedError()
