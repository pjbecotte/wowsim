from dataclasses import dataclass
from typing import List

from wow_decision_nodes.value import WowValue


@dataclass
class Event:
    time: float
    value: WowValue


class Aura:
    duration = 5.0

    def __init__(self, expires_at: float, events: List[Event]):
        self.expires_at = expires_at
        self.events = events

    def copy(self):
        return self.__class__(self.expires_at, self.events)

    @classmethod
    def create(cls, creation_time):
        return cls(creation_time + cls.duration, cls.new_events())

    @staticmethod
    def new_events():
        raise NotImplementedError()

    def resolve(self, current_time):
        events = []
        value = WowValue(0, 0)
        for event in self.events:
            if current_time > event.time:
                value += event.value
            else:
                events.append(event)
        return value
