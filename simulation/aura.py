from typing import List

from simulation.events import Event


class Aura:
    def __init__(self, expires, events: None):
        self.events: List[Event] = events or []
        self.expires = expires

    def resolve_events(self, timestamp):
        for event in self.events:
            if event.timestamp <= timestamp:
                yield event
