from random import choice
from uuid import uuid4

from sqlalchemy import create_engine, Column, String, JSON, update, INT
from sqlalchemy.orm import declarative_base, Session


engine = create_engine("sqlite+pysqlite:///sqlite.db", echo=True, future=True)
Base = declarative_base()


class StateRecorder(Base):
    __tablename__ = "state"
    primary_key = Column(INT, primary_key=True)
    execution_id = Column(String(32))
    decisions = Column(JSON)
    state = Column(JSON)
    next_decision = Column(String(64))
    end_result = Column(JSON)


Base.metadata.create_all(engine)


class State:
    def __init__(self):
        self.execution_id = uuid4().hex
        self.decisions = []

    def run_step(self):
        next_step = choice(self.possible_choices())
        self.record_step(next_step)
        self.decisions.append(next_step)
        self.execute_action(next_step)

    def finished(self):
        return len(self.decisions) > 10

    def execute_action(self, action):
        return

    def possible_choices(self):
        return f"something {choice([1, 2, 3])}"

    def record_step(self, next_step):
        with Session(engine) as session:
            session.add(
                StateRecorder(
                    execution_id=self.execution_id,
                    decisions=self.decisions,
                    next_decision=next_step,
                    state=self.get_state(),
                )
            )
            session.commit()

    def get_state(self):
        return {"prop": choice([1, 2, 3]), "props2": choice([2, 4, 6, 8])}

    def final_result(self):
        return {"value": 42}

    def update_results(self):
        with Session(engine) as session:
            session.execute(
                update(StateRecorder)
                .where(StateRecorder.execution_id == self.execution_id)
                .values(end_result=self.final_result())
            )
            session.commit()
