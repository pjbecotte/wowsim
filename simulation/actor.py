from typing import Dict

from simulation.aura import Aura


class Actor:
    def __init__(self):
        self.auras: Dict[str, Aura] = {}

    def resolve_auras(self, elapsed_time):
        for name, aura in self.auras.items():
            for event in aura.resolve_events(elapsed_time):
                yield event
            if aura.expires <= elapsed_time:
                del self.auras[name]
